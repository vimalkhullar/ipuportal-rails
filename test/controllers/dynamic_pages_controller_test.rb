require 'test_helper'

class DynamicPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get course" do
    get :course
    assert_response :success
  end

  test "should get college" do
    get :college
    assert_response :success
  end

  test "should get staff" do
    get :staff
    assert_response :success
  end

end
