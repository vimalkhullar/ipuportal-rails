class College < ActiveRecord::Base
	before_save   :upcase_name
	mount_uploader :picture, PictureUploader
	validates :name, :presence => true

	has_many :ranks
	has_many :courses, :through => :ranks

	def upcase_name
      self.name = name.upcase
    end
	
end
