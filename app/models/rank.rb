class Rank < ActiveRecord::Base
	belongs_to :course
	belongs_to :college

	validates :course_id, presence: true
	validates :college_id, presence: true
  	validates :dgen, presence: true
  	validates :odgen, presence: true

	validates :dgen, :numericality => { :greater_than => 0 }
	validates :odgen, :numericality => { :greater_than => 0 }

	
end
