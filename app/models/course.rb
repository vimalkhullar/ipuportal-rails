class Course < ActiveRecord::Base
	before_save   :upcase_name
	validates :name, :presence => true
	validates :eligibility, :presence => true
	validates :duration, :presence => true

	has_many :ranks
	has_many :colleges, :through => :ranks

	def upcase_name
      self.name = name.upcase
    end
	
end

