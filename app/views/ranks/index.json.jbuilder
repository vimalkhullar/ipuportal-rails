json.array!(@ranks) do |rank|
  json.extract! rank, :id, :college_id, :course_id, :dgen, :odgen
  json.url rank_url(rank, format: :json)
end
