json.array!(@courses) do |course|
  json.extract! course, :id, :name, :eligibility, :duration
  json.url course_url(course, format: :json)
end
