json.array!(@colleges) do |college|
  json.extract! college, :id, :name, :address, :phone
  json.url college_url(college, format: :json)
end
