class DynamicPagesController < ApplicationController

def rank_check
    @rank = params[:dynamic_pages][:rank]
    @course_id = params[:dynamic_pages][:course_id]
    @category = params[:dynamic_pages][:category]

      if @category == 'DGEN'
        @result = College.select(:name).where("dgen >= ?", @rank).where("course_id = ?", @course_id).order("ranks.dgen").joins(:ranks)
      else
        
        @result = College.select(:name).where("odgen >= ?", @rank).where("course_id = ?", @course_id).order("ranks.odgen").joins(:ranks)
      end 
     
end

def course_check
    @course = Course.find(params[:dynamic_pages][:id])
       
end

def college_check
    @college = College.find(params[:dynamic_pages][:id])

end

end