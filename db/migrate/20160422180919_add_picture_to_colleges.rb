class AddPictureToColleges < ActiveRecord::Migration
  def change
    add_column :colleges, :picture, :string
  end
end
