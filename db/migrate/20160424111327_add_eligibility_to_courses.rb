class AddEligibilityToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :eligibility, :string
  end
end
