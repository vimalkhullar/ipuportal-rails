class CreateRanks < ActiveRecord::Migration
  def change
    create_table :ranks do |t|
      t.integer :college_id, :null => false
      t.integer :course_id, :null => false
      t.integer :dgen, :null => false
      t.integer :odgen, :null => false

      t.timestamps null: false
    end
  end
end
